# H2Tools

## A Python module for constructing, manipulating, and visualizing patterns in the Poincare disc model of hyperbolic two-space

### Introduction

H2Tools is a Python module built around the Poincare disc model for visualizing embeddings of graphs in hyperbolic space. The module represents coordinates by complex numbers and hosts a number of mathematical operations for reflecting, rotating, or translating points.

The central data structure is a HyperbolicStructure() which consists of the points and edges of the graph in question. Points and edges have a number of properties that can be set in order to produce various visualizations. A set of such hyperbolic structures can eventually be plotted on the Poincare disc using the provided plotting functions.

### Dependencies

The plotting is based on the Python modules matplotlib and numpy which must be installed. Some of the examples found in /Examples/ utilize the module openmesh for handling meshes using the .off-format. This module is not required for basic use.

### Installation

H2Tools is designed as a pip-installable Python module. To configure H2Tools on your system, clone the repositor with e.g.:
```console
git clone https://gitlab.com/mcpe/H2Tools.git /Path/To/Put/Repository
```
and run this command from the root of the directory into which the project was cloned:
```console
cd /Path/To/Put/Repository
pip install .
```

### Brief examples

#### Building a pattern from scratch

```python
from H2Tools import *

# Start with an empty pattern
NewPattern = HyperbolicStructure()

# Add some points
NewPattern.AddPoint(Point( 0.2 + 0.2j, ID = 1))
NewPattern.AddPoint(Point( 0.2 - 0.2j, ID = 2))
NewPattern.AddPoint(Point(-0.2 - 0.2j, ID = 3))
NewPattern.AddPoint(Point(-0.2 + 0.2j, ID = 4))

# And some edges
NewPattern.AddEdge(Edge((1, 2), ID = 1))
NewPattern.AddEdge(Edge((2, 3), ID = 2))
NewPattern.AddEdge(Edge((3, 4), ID = 3))
NewPattern.AddEdge(Edge((4, 1), ID = 4))

# And output the result to a file
OutputH2P('NewPattern.h2p', NewPattern)
```
	
#### Importing, altering, and plotting a H2P file

```python
from H2Tools import *

ImportedPattern = ImportH2P('Dodecagon.h2p')
Origin          = Point(0.0 + 0.0j, ID = 0)

# Rotate the pattern around the origin by 0.1 * pi
for Point in ImportedPattern.Points.values():
	RotatedPosition = RotatePoint(Point, Origin, 0.1 * 3.14159)
	Point.SetPosition(RotatedPosition)

# And change the "color" of the edges
for Edge in ImportedPattern.Edges.values():
	Edge.SetType(2)

# And plot the resulting structure
PlotPoincareDisk('Image.png', ImportedPattern)
```

### Reference

If you use H2Tools in your work, please cite:

Exploring hyperbolic order in curved materials <br>
Pedersen, Hyde, Ramsden, and Kirkensgaard (2023) <br>
Soft Matter 19, 1586-1595
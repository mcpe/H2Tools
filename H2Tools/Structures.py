import sys

################################
# Class for storing point info #
################################
class Point:
	"""
	The class describes a point in the Poincare disc model with the properties below along with methods to manipulate the properties of these.

	Keyword arguments:
	Position -- a complex number with modulus below 1
	ID -- an integer (default is None)
	Type -- anything (default is None)
	Radius -- a float (default is None)
	"""

	def __init__(self, Position, ID, Type = None, Radius = None):
		assert abs(Position) < 1.0, 'Cannot assign coordinates outside the open unit disk to point!'
		assert type(ID) == int, 'Error while adding point with ID {}. ID must be an integer.'.format(ID)
		
		self.Position = Position
		self.ID       = ID
		self.Type     = Type
		self.Radius   = Radius
		
	def SetPosition(self, Position):
		assert abs(Position) < 1.0, 'Cannot assign coordinates outside the open unit disk to point!'

		self.Position = Position
		
	def SetType(self, Type):
		self.Type = Type
		
	def SetRadius(self, Radius):
		self.Radius = Radius
		
	def MinkowskiCoordinates(self):
		Scale = 1.0 / (1.0 - abs(self.Position)**2)

		MinkowskiPoint = (Scale * 2.0 * self.Position.real, Scale * 2.0 * self.Position.imag, Scale * (1.0 + abs(self.Position)**2))

		return MinkowskiPoint
	
	def HomogenousCoordinates(self):
		x, y, z = self.MinkowskiCoordinates()
		
		assert z != 0.0, 'Minkowski point with z = 0 cannot be passed to conversion function!'
	
		HomogenousPoint = (x / z, y / z, 1.0)
	
		return HomogenousPoint
		
	def PrintInfo(self):
		sys.stdout.write('Position: {}, ID: {}, Type: {}, Radius: {} \n'.format(self.Position, self.ID, self.Type, self.Radius))

###############################
# Class for storing edge info #
###############################
class Edge:
	"""
	The class describes a edge in a hyperbolic structure along with methods for manipulating these.

	Keyword arguments:
	Points -- a tuple (ID1, ID2) containing the IDs of the two points connected by the edge
	ID -- an integer used as the ID of the point
	Type -- anything (default is None)
	Width -- a float (default is None)
	Linestyle -- a string with a matplotlib linestyle (default is None)
	"""

	def __init__(self, Endpoints, ID, Type = None, Width = None, Linestyle = None):
		assert type(ID) == int, 'Error while adding edge with ID {}. ID must be an integer.'.format(ID)
		assert type(Endpoints) == tuple, 'Error while adding edge with endpoints {}. Endpoints must be a tuple of IDs.'.format(Endpoints)
		
		self.Endpoints = Endpoints
		self.ID        = ID
		self.Type      = Type
		self.Width     = Width
		self.Linestyle = Linestyle
		
	def SetEndpoints(self, Endpoints):
		self.Endpoints = Endpoints
		
	def SetType(self, Type):
		self.Type = Type
		
	def SetWidth(self, Width):
		self.Width = Width
		
	def SetLinestyle(self, Linestyle):
		self.Linestyle = Linestyle
		
	def PrintInfo(self):
		sys.stdout.write('Endpoints: {}, ID: {}, Type: {}, Width: {}, Linestyle: {} \n'.format(self.Endpoints, self.ID, self.Type, self.Width, self.Linestyle))

##############
# Main class #
##############
class HyperbolicStructure:
	"""
	The class is the main container of hyperbolic structures. It contains lists of points and edges in the structure as well as the methods needed to manipulate these.

	Keyword arguments:
	Points -- an Python dictionary of instances of H2Tools.Point
	Edges -- an Python dictionary of instances of H2Tools.Edge
	PlotEdges -- a boolean (default is True)
	Colors -- a Python dictionary mapping types to matplotlib colors (default is {None: 'black', '1': 'red', '2': 'blue', '3': 'green'})
	"""

	def __init__(self, Points = {}, Edges = {}, PlotEdges = True, Colors = {None: 'black', '1': 'red', '2': 'blue', '3': 'green'}):
		self.Points    = Points
		self.Edges     = Edges
		self.PlotEdges = PlotEdges
		self.Colors    = Colors
		
	def AddPoint(self, Point):
		self.Points[Point.ID] = Point
	
	def AddEdge(self, Edge):
		ListOfPointIDs = self.Points.keys()
	
		assert Edge.Endpoints[0] in ListOfPointIDs, 'Error while adding edge with endpoints {}. The first point is not in the pattern.'
		assert Edge.Endpoints[1] in ListOfPointIDs, 'Error while adding edge with endpoints {}. The second point is not in the pattern.'
	
		self.Edges[Edge.ID] = Edge
		
	def DeletePoint(self, PointID):
		del self.Points[PointID]
		
		for EdgeID in self.Edges.keys():
			
			if PointID in self.Edges[EdgeID].Points:
				del self.Edges[EdgeID]
		
	def DeleteEdge(self, EdgeID):
		del self.Edges[EdgeID]

	def NumberOfPoints(self):
		return len(self.Points)
		
	def NumberOfEdges(self):
		return len(self.Edges)

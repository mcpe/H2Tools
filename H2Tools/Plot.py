import matplotlib.pyplot
import os
import math
import sys
import numpy
from .Geometry import *
from .Structures import *

########################
# Supporting functions #
########################
def CalculateEuclideanDisc(CurrentPoint):
	# Place the node on the disk
	DistanceFromCenter = Distance(Point(0.0, ID = 0), CurrentPoint)

	if DistanceFromCenter > sys.float_info.epsilon:
		OutermostPoint  = InterpolatePosition(Point(0.0, ID = 0), CurrentPoint, (DistanceFromCenter + CurrentPoint.Radius) / DistanceFromCenter)
		InnermostPoint  = InterpolatePosition(Point(OutermostPoint, ID = 0), CurrentPoint, 2.0)
		EuclideanCenter = ((OutermostPoint.real + InnermostPoint.real) / 2.0, (OutermostPoint.imag + InnermostPoint.imag) / 2.0)
		EuclideanRadius = math.hypot(OutermostPoint.real - EuclideanCenter[0], OutermostPoint.imag - EuclideanCenter[1])

	else:
		EuclideanCenter = (0.0, 0.0)
		Temp            = (numpy.cosh(CurrentPoint.Radius) - 1.0) / 2.0
		EuclideanRadius = (Temp / (1.0 + Temp))**0.5

	# Return Euclidean properties
	return EuclideanCenter[0], EuclideanCenter[1], EuclideanRadius

def CalculateGeodesic(x1, y1, x2, y2, Color, Linewidth, Linestyle, zOrder):
	Determinant = x1 * y2 - x2 * y1

	CenterX = ((1.0 + math.pow(x1, 2)) * y2 + math.pow(y1, 2) * y2 - y1 * (1.0 + math.pow(x2, 2) + math.pow(y2, 2))) / (-2.0 * x2 * y1 + 2.0 * x1 * y2)
	CenterY =   (x2 + math.pow(x1, 2)  * x2 + x2 * math.pow(y1, 2) - x1 * (1.0 + math.pow(x2, 2) + math.pow(y2, 2))) /  (2.0 * x2 * y1 - 2.0 * x1 * y2)

	if math.pow(CenterX, 2) + math.pow(CenterY, 2) > 1.0:
		Radius = math.sqrt(math.pow(CenterX, 2) + math.pow(CenterY, 2) - 1.0)

	else:
		Radius = sys.float_info.epsilon

	ThetaPoint1 = math.acos(((x1 - CenterX) * 1.0 + (y1 - CenterY) * 0.0) / math.sqrt(math.pow(x1 - CenterX, 2) + math.pow(y1 - CenterY, 2))) 
	ThetaPoint2 = math.acos(((x2 - CenterX) * 1.0 + (y2 - CenterY) * 0.0) / math.sqrt(math.pow(x2 - CenterX, 2) + math.pow(y2 - CenterY, 2)))

	if y1 - CenterY < 0.0:
		ThetaPoint1 *= -1.0 

	if y2 - CenterY < 0.0:
		ThetaPoint2 *= -1.0

	if Determinant < 0.0:
		Theta1 = ThetaPoint1
		Theta2 = ThetaPoint2
	
	else:
		Theta1 = ThetaPoint2
		Theta2 = ThetaPoint1

	Geodesic = matplotlib.patches.Arc((CenterX, CenterY), 2.0 * Radius, 2.0 * Radius, theta1 = math.degrees(Theta1), theta2 = math.degrees(Theta2), color = Color, linewidth = Linewidth, linestyle = Linestyle, zorder = zOrder)

	return Geodesic

##################################
# Function for plotting patterns #
##################################
def PlotPoincareDisk(Filename, ListOfHyperbolicStructure, DefaultNodeSize = 10.0, DefaultLinewidth = 1.0, NodeAlpha = 0.50, DiskColor = "black"):
	"""
	The function outputs a matplotlib image from the supplied list of hyperbolic structures.

	Keyword arguments:
	Filename -- string
	ListOfHyperbolicStructure -- a (list of) instances of H2Tools.HyperbolicStructure
	DefaultNodeSize -- a float (default is 10.0)
	DefaultLinewidth -- a float (default is 1.0)
	NodeAlpha -- a float (default is 0.50)
	DiskColor -- a string (default is "black")
	"""
	
	# Force input to be a list
	if type(ListOfHyperbolicStructure) is not list: ListOfHyperbolicStructure = [ListOfHyperbolicStructure]
	
	# Initialise Poincare disk model
	Figure = matplotlib.pyplot.figure(figsize = (20, 20))
	Addition = Figure.add_subplot(1, 1, 1)
	UnitCircle = matplotlib.pyplot.Circle((0.0, 0.0), radius = 1.0, color = DiskColor, fill = True, alpha = 0.1, linewidth = 2.0)
	Addition.add_patch(UnitCircle)
	
	# Loop over all the passed structures
	for HyperbolicStructure in ListOfHyperbolicStructure:
		xList      = []
		yList      = []
		ColorList  = []
		RadiusList = []
		Positions  = {}

		# Plot points
		for CurrentPoint in HyperbolicStructure.Points.values():		
			ColorList.append(HyperbolicStructure.Colors.get(CurrentPoint.Type, 'black'))
			Positions[CurrentPoint.ID] = (CurrentPoint.Position.real, CurrentPoint.Position.imag)

			if CurrentPoint.Radius == None:
				xList.append(CurrentPoint.Position.real)
				yList.append(CurrentPoint.Position.imag)
				RadiusList.append(DefaultNodeSize)
				
			else:
				xEuclidean, yEuclidean, RadiusEuclidean = CalculateEuclideanDisc(CurrentPoint)
				xList.append(xEuclidean)
				yList.append(yEuclidean)
				
				# Empirical factor working at this exact figure size
				RadiusList.append(4.0 * math.pi * RadiusEuclidean**2 * 95000)

		matplotlib.pyplot.scatter(xList, yList, s = RadiusList, c = ColorList, alpha = NodeAlpha, zorder = 1)

		# Plot edges
		if HyperbolicStructure.PlotEdges:
		
			for Edge in HyperbolicStructure.Edges.values():		
				# Gather properties of edge	
				(x1, y1) = Positions[Edge.Endpoints[0]]
				(x2, y2) = Positions[Edge.Endpoints[1]]
				Color    = HyperbolicStructure.Colors.get(Edge.Type, 'black')
				
				if Edge.Width == None:
					EdgeWidth = DefaultLinewidth
				else:
					EdgeWidth = Edge.Width

				if Edge.Linestyle == None:
					Linestyle = 'solid'
				else:
					Linestyle = Edge.Linestyle

				# Plot each edge
				if math.hypot(x2 - x1, y2 - y1) > 1.0e-10:#sys.float_info.epsilon:

					if math.fabs(x1 * y2 - x2 * y1) > 1.0e-10:#sys.float_info.epsilon:
						Geodesic = CalculateGeodesic(x1, y1, x2, y2, Color, EdgeWidth, Linestyle, zOrder = 4)
						Addition.add_patch(Geodesic)

					else:
						matplotlib.pyplot.plot((x1, x2), (y1, y2), linewidth = EdgeWidth, color = Color, linestyle = Linestyle, zorder = 4)
					
	# Scale and save figure
	matplotlib.pyplot.xlim(-1.01, 1.01)
	matplotlib.pyplot.ylim(-1.01, 1.01)
	matplotlib.pyplot.axis('off')
	matplotlib.pyplot.savefig(Filename)
	matplotlib.pyplot.close('all')

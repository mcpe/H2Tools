import math
import cmath
import sys
from .Structures import *

def MobiusIsometry(z, a, Theta):
	"""
	The function calculates the Mobius transformation defined by a and Theta of the point z:
	f(z) = exp(i * Theta) * (z - a) / (a* * z - 1)

	Keyword arguments:
	z -- an instance of H2Tools.Point
	a -- an instance of H2Tools.Point
	Theta -- a float
	"""
	
	return cmath.rect(1.0, Theta) * (z.Position - a.Position) / (a.Position.conjugate() * z.Position - 1.0)

def Distance(PointA, PointB):
	"""
	The function calculates the distance between two points in the open disc using the metric of the Poincare disc model.

	Keyword arguments:
	PointA -- an instance of H2Tools.Point
	PointB -- an instance of H2Tools.Point
	"""
	
	Nominator   = (PointA.Position.real - PointB.Position.real)**2 + (PointA.Position.imag - PointB.Position.imag)**2
	Denominator = (1.0 - abs(PointA.Position)**2) * (1.0 - abs(PointB.Position)**2)
	Delta       = 2.0 * Nominator / Denominator
	Distance    = math.acosh(1.0 + Delta)

	return Distance
	
def InterpolatePosition(PointA, PointB, k):
	"""
	Starting from PointA, the function calculates the position along the geodesic from PointA in the direction of PointB, the distance to PointA of which is a factor k times that of the distance from PointA to PointB.

	Keyword arguments:
	PointA -- an instance of H2Tools.Point
	PointB -- an instance of H2Tools.Point
	k -- a positive float (or zero)
	"""
	
	assert k >= 0.0, 'k passed to InterpolatePosition must be greater than or equal to zero.'
	
	# Transform point A to the origin
	TransformedA = Point(0.0, ID = 0)
	TransformedB = Point(MobiusIsometry(PointB, PointA, 0.0), ID = 0)
	TransformedO = Point(MobiusIsometry(Point(0.0, ID = 0), PointA, 0.0), ID = 0)
		
	# Find the interpolated position
	OB = Distance(TransformedB, Point(0.0, ID = 0))
	Theta = cmath.phase(TransformedB.Position)
	EuclidianDistance = math.sqrt(math.cosh(k * OB) - 1.0) / math.sqrt(math.cosh(k * OB) + 1.0)
	TransformedInterpolatedPosition = complex(math.cos(Theta) * EuclidianDistance, math.sin(Theta) * EuclidianDistance)

	# Transform the point back
	InterpolatedPosition = MobiusIsometry(Point(TransformedInterpolatedPosition, ID = 0), TransformedO, 0.0)

	return InterpolatedPosition
	
def RotatePoint(PointToRotate, PointOfRotation, Theta):
	"""
	The function calculates the position of the position of PointToRotate when rotated by the angle Theta around the position of PointOfRotation.

	Keyword arguments:
	PointToRotate -- an instance of H2Tools.Point
	PointOfRotation -- an instance of H2Tools.Point
	Theta -- a float
	"""
	
	# Translate rotational center to the origin
	TransformedPoint  = Point(MobiusIsometry(PointToRotate, PointOfRotation, 0.0), ID = 0)
	TransformedOrigin = Point(MobiusIsometry(Point(0.0, ID = 0), PointOfRotation, 0.0), ID = 0)

	# Rotate point
	TransformedPoint = Point(TransformedPoint.Position * cmath.rect(1.0, Theta), ID = 0)

	# Transform the point back
	RotatedPoint = MobiusIsometry(TransformedPoint, TransformedOrigin, 0.0)

	return RotatedPoint

def ReflectPointInGeodesic(PointOfInterest, GeodesicPoint1, GeodesicPoint2):
	"""
	The function calculates the reflection of PointOfInterest in the hyperbolic geodesic containing GeodesicPoint1 and GeodesicPoint2.

	Keyword arguments:
	PointOfInterest -- an instance of H2Tools.Point
	GeodesicPoint1 -- an instance of H2Tools.Point
	GeodesicPoint2 -- an instance of H2Tools.Point
	"""
	
	# Translate Point 1 to the origin
	TranslatedPointOfInterest = Point(MobiusIsometry(PointOfInterest   , GeodesicPoint1, 0.0), ID = 0)
	TranslatedGeodesicPoint2  = Point(MobiusIsometry(GeodesicPoint2    , GeodesicPoint1, 0.0), ID = 0)
	TranslatedOrigin          = Point(MobiusIsometry(Point(0.0, ID = 0), GeodesicPoint1, 0.0), ID = 0)

	# Rotate Point 2 to the x-axis
	Theta = cmath.phase(TranslatedGeodesicPoint2.Position)

	RotatedPointOfInterest = Point(TranslatedPointOfInterest.Position * cmath.rect(1.0, -Theta), ID = 0)

	# Reflect point
	ReflectedPointOfInterest = Point(RotatedPointOfInterest.Position.conjugate(), ID = 0)

	# Rotate Poincare disk back
	RotatedPointOfInterest = Point(ReflectedPointOfInterest.Position * cmath.rect(1.0, Theta), ID = 0)

	# Reflect the point back
	ReflectedPoint = MobiusIsometry(RotatedPointOfInterest, TranslatedOrigin, 0.0)

	return ReflectedPoint

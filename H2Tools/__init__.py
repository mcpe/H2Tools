"""
H2Tools

A Python module for constructing, manipulating and visualizing patterns in the Poincare disc model of hyperbolic two-space.
"""

from .Structures import *
from .Geometry import *
from .IO import *
from .Plot import *

__version__ = "0.1.0"
__author__ = 'Martin Cramer Pedersen'
__reference__ = 'Name\nPedersen, Hyde, Ramsden, and Kirkensgaard\nJournal.'

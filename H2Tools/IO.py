import sys
from .Structures import *

################################
# Function for importing files #
################################
def ImportH2P(Filename):
	"""
	The function imports a file with the H2P format into an instance of H2Tools.HyperbolicStructure.

	Keyword arguments:
	Filename -- a string
	"""
	
	File = open(Filename)
	Data = File.readlines()
	File.close()
	
	NewHyperbolicStructure = HyperbolicStructure(Points = {}, Edges = {})

	for Line in Data:
	
		if Line[0] == 'P':
			Split = Line.split()

			PointID = int(Split[1]) if Split[1] != 'None' else None
			x       = float(Split[2])
			y       = float(Split[3])
			Type    = Split[4] if Split[4] != 'None' else None
			Radius  = float(Split[5]) if Split[5] != 'None' else None

			NewHyperbolicStructure.AddPoint(Point(complex(x, y), PointID, Type, Radius))
			
		elif Line[0] == 'E':
			Split = Line.split()

			EdgeID    = int(Split[1]) if Split[1] != 'None' else None
			Point1    = int(Split[2])
			Point2    = int(Split[3])
			Type      = Split[4] if Split[4] != 'None' else None
			Width     = float(Split[5]) if Split[5] != 'None' else None
			Linestyle = Split[6] if Split[6] != 'None' else None
			
			NewHyperbolicStructure.AddEdge(Edge((Point1, Point2), EdgeID, Type, Width, Linestyle))
			
		else:
			sys.stdout.write('Skipping line: {}\n'.format(Line))
			
	sys.stdout.write('Imported {} points and {} edges from {}.\n'.format(NewHyperbolicStructure.NumberOfPoints(), NewHyperbolicStructure.NumberOfEdges(), Filename))
	
	return NewHyperbolicStructure

################################
# Function for exporting files #
################################
def OutputH2P(Filename, HyperbolicStructure):
	"""
	The function writes a file with the H2P format containing the information in the supplied hyperbolic structure.

	Keyword arguments:
	Filename -- a string
	HyperbolicStructure -- an instance of H2Tools.HyperbolicStructure
	"""
	
	Outputfile = open(Filename, 'w')

	for Point in HyperbolicStructure.Points.values():
		Outputfile.write('P {} {} {} {} {} \n'.format(Point.ID, Point.Position.real, Point.Position.imag, Point.Type, Point.Radius))

	for Edge in HyperbolicStructure.Edges.values():
		Outputfile.write('E {} {} {} {} {} {}\n'.format(Edge.ID, Edge.Endpoints[0], Edge.Endpoints[1], Edge.Type, Edge.Width, Edge.Linestyle))

	Outputfile.close()
	
	sys.stdout.write('Exported {} points and {} edges to {}.\n'.format(HyperbolicStructure.NumberOfPoints(), HyperbolicStructure.NumberOfEdges(), Filename))

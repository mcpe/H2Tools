# H2Tools - Meshing script

## Introduction

This folder contains a C++ script used for constructing triangular meshes fra a point set embedded in Euclidean three-space. The input point set must be confined to the unit cube.

## Usage

The script compiled using a compiler such as g++ properly linked to eigen3, CGAL and a few other libraries:

```
g++ Meshing.cpp -o Meshing.sh -I/usr/include/eigen3/ -lCGAL -lCGAL_Core -lgmpxx -lgmp -lmpfr
```

takes the following, ordered inputs:

| Argument | Description                                       | Suggested value |
| -------- | ------------------------------------------------- | --------------- |
| 1        | Path to the point set in .xyz-format              | -               |
| 2        | Path to the output in .off-format                 | -               |
| 3        | Percentage of outliers to remove                  | 0.0             |
| 4        | Size of neighborhood used in smoothing            | 20              |
| 5        | Sharpness angle passed to the smoothing algorithm | 90.0            |
| 6        | Number of interations of the smoothing process    | 5               |
| 7        | Facet size in the final mesh                      | 0.01            |

and outputs a mesh approximating the geometry of the given point set along with several files describing the intermediate steps. 

The meshing algorithm is part of the CGAL library and is described in detail here:

Periodic meshes for the cgal library (2014)<br>
Pelle and Teillaud<br>
23rd Int. Meshing Roundtable (IMR23)

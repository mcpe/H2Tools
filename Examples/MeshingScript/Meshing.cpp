#define CGAL_EIGEN3_ENABLED

#include <cmath>
#include <fstream>
#include <iostream>
#include <utility>
#include <deque>
#include <vector>

#include <CGAL/bilateral_smooth_point_set.h>
#include <CGAL/config.h>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/IO/read_xyz_points.h>
#include <CGAL/IO/write_xyz_points.h>
#include <CGAL/make_periodic_3_mesh_3.h>
#include <CGAL/Mesh_complex_3_in_triangulation_3.h>
#include <CGAL/Mesh_criteria_3.h>
#include <CGAL/mst_orient_normals.h>
#include <CGAL/optimize_periodic_3_mesh_3.h>
#include <CGAL/pca_estimate_normals.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/Periodic_3_function_wrapper.h>
#include <CGAL/Periodic_3_mesh_3/config.h>
#include <CGAL/Periodic_3_mesh_3/IO/File_medit.h>
#include <CGAL/Periodic_3_mesh_triangulation_3.h>
#include <CGAL/Point_with_normal_3.h>
#include <CGAL/Poisson_reconstruction_function.h>
#include <CGAL/property_map.h>
#include <CGAL/remove_outliers.h>
#include <CGAL/Simple_cartesian.h>
#include <CGAL/tags.h>
#include <CGAL/Labeled_mesh_domain_3.h>

// Concurrency tag
#ifdef CGAL_LINKED_WITH_TBB
	typedef CGAL::Parallel_tag Concurrency_tag;
#else
	typedef CGAL::Sequential_tag Concurrency_tag;
#endif

// Domain
typedef CGAL::Exact_predicates_inexact_constructions_kernel Kernel;
typedef CGAL::Point_with_normal_3<Kernel> Point_with_normal;
typedef std::deque<Point_with_normal> PointList;
typedef Kernel::Point_3 Point;
typedef Kernel::Vector_3 Vector;
typedef std::pair<Point, Vector> PointVectorPair;
typedef Kernel::FT (Function)(const Point&);
typedef CGAL::Poisson_reconstruction_function<Kernel> PoissonReconstructionFunctionTemplate;

// Triangulation and meshing
typedef CGAL::Labeled_mesh_domain_3<Kernel> PeriodicMeshDomain;
typedef CGAL::Periodic_3_mesh_triangulation_3<PeriodicMeshDomain>::type Triangulation;
typedef CGAL::Mesh_complex_3_in_triangulation_3<Triangulation> MeshFromTriangulationTemplate;
typedef CGAL::Mesh_criteria_3<Triangulation> MeshCriteriaTemplate;
typedef CGAL::Periodic_3_function_wrapper<Function, Kernel> PeriodicFunction;

// Global function wrapper
std::function <const Kernel::FT (const Point&)> FunctionWrapper;

Kernel::FT WrappedFunction(const Point& p) {
    Kernel::FT ReturnValue = FunctionWrapper(p);
    return ReturnValue;
}

// Program
int main(int argc, char** argv) {
    /*************************
	* Command line arguments *
    **************************/
    int i;
    
    // Filenames
    std::string InputFilename  = argv[1];
    std::string OutputFilename = argv[2];
    
    // For sorting
    const double PercentageOfPointsToRemove = std::stof(argv[3]);
	const int NumberOfNeighbors             = std::stoi(argv[4]);
	
	// For smoothing
	const double SharpnessAngle  = std::stof(argv[5]);
	const int NumberOfIterations = std::stoi(argv[6]);
	
	// For meshing
	const double FacetSize  = std::stof(argv[7]);

    /********
	* Input *
    ********/
	// Version number
	std::cout << "My CGAL library is version " <<  CGAL_VERSION_STR << "." << std::endl;

    // Read an .xyz point set file
	std::vector<Point> Points;
	std::ifstream InputStream(InputFilename);

	if (!InputStream || !CGAL::read_xyz_points(InputStream, std::back_inserter(Points))) {
		std::cerr << "Error: cannot read file " << InputFilename << std::endl;
		return EXIT_FAILURE;
	}
	
	/******************
	* Remove outliers *
    ******************/
	// Remove
	Points.erase(CGAL::remove_outliers(Points, NumberOfNeighbors, CGAL::parameters::threshold_percent(PercentageOfPointsToRemove)), Points.end());
	std::vector<Point>(Points).swap(Points);

	// Output
    std::ofstream OutputStream(OutputFilename + ".out.xyz");

    if (!OutputStream || !CGAL::write_xyz_points(OutputStream, Points)) {
		return EXIT_FAILURE;
    }
    
    // Conclude algorithm
	std::cout << "Importing and sorting concluded correctly - acquired " << Points.size() << " points." << std::endl;
	
	/***********
	* Re-input *
    ***********/
    // Read an .xyz point set file
	std::vector<PointVectorPair> PointsWithNormals;
	std::ifstream NormalsInputStream(OutputFilename + ".out.xyz");

	if (!NormalsInputStream || !CGAL::read_xyz_points(NormalsInputStream, std::back_inserter(PointsWithNormals), CGAL::parameters::point_map(CGAL::First_of_pair_property_map<PointVectorPair>()).normal_map(CGAL::Second_of_pair_property_map<PointVectorPair>()))) {
		std::cerr << "Error: cannot read file " << OutputFilename + ".out.xyz" << std::endl;
		return EXIT_FAILURE;
	}
	
	/**********
	* Normals *
    **********/
    // Estimates normals direction
    CGAL::pca_estimate_normals<Concurrency_tag>(PointsWithNormals, NumberOfNeighbors, CGAL::parameters::point_map(CGAL::First_of_pair_property_map<PointVectorPair>()).normal_map(CGAL::Second_of_pair_property_map<PointVectorPair>()));

    // Orients normals
    std::vector<PointVectorPair>::iterator InteratorUnorientedPoints = CGAL::mst_orient_normals(PointsWithNormals, NumberOfNeighbors, CGAL::parameters::point_map(CGAL::First_of_pair_property_map<PointVectorPair>()).normal_map(CGAL::Second_of_pair_property_map<PointVectorPair>()));

    // Delete points with an unoriented normal
    PointsWithNormals.erase(InteratorUnorientedPoints, PointsWithNormals.end());

	// Output
    std::ofstream NormalsOutputStream(OutputFilename + ".nor.xyz");

    if (!OutputStream || !CGAL::write_xyz_points(NormalsOutputStream, PointsWithNormals, CGAL::parameters::point_map(CGAL::First_of_pair_property_map<PointVectorPair>()).normal_map(CGAL::Second_of_pair_property_map<PointVectorPair>()))) {
		return EXIT_FAILURE;
    }

	// Conclude algorithm
	std::cout << "Normals generated concluded correctly - " << PointsWithNormals.size() << " PointsWithNormals remaining." << std::endl;
	
	/***********
	* Re-input *
    ***********/
    // Read an .xyz point set file
	std::vector<PointVectorPair> SmoothPoints;
	std::ifstream SmoothInputStream(OutputFilename + ".nor.xyz");

	if (!SmoothInputStream || !CGAL::read_xyz_points(SmoothInputStream, std::back_inserter(SmoothPoints), CGAL::parameters::point_map(CGAL::First_of_pair_property_map<PointVectorPair>()).normal_map(CGAL::Second_of_pair_property_map<PointVectorPair>()))) {
		std::cerr << "Error: cannot read file " << OutputFilename + ".nor.xyz" << std::endl;
		return EXIT_FAILURE;
	}
	
	/************
	* Smoothing *
    ************/
	for (i = 0; i < NumberOfIterations; ++i) {
		CGAL::bilateral_smooth_point_set<Concurrency_tag>(SmoothPoints, NumberOfNeighbors, CGAL::parameters::point_map(CGAL::First_of_pair_property_map<PointVectorPair>()).normal_map(CGAL::Second_of_pair_property_map<PointVectorPair>()).sharpness_angle (SharpnessAngle));
	}

	// Output points
    std::ofstream SmoothOutputStream(OutputFilename + ".smo.xyz");

    if (!OutputStream || !CGAL::write_xyz_points(SmoothOutputStream, SmoothPoints, CGAL::parameters::point_map(CGAL::First_of_pair_property_map<PointVectorPair>()).normal_map(CGAL::Second_of_pair_property_map<PointVectorPair>()))) {
		return EXIT_FAILURE;
    }

	// Conclude algorithm
	std::cout << "Smoothing algorithm concluded correctly - " << SmoothPoints.size() << " points remaining." << std::endl;
	
    /***********
	* Re-input *
    ***********/
    // Read an .xyz point set
	PointList FinalPoints;
	std::ifstream FinalInputStream(OutputFilename + ".smo.xyz");

	if (!FinalInputStream || !CGAL::read_xyz_points(FinalInputStream, std::back_inserter(FinalPoints), CGAL::parameters::normal_map(CGAL::make_normal_of_point_with_normal_map(PointList::value_type())))) {
		std::cerr << "Error: cannot read file " << OutputFilename + ".smo.xyz" << std::endl;
		return EXIT_FAILURE;
	}

    /**********
	* Meshing *
    **********/
	// Initiate surface reconstruction
    PoissonReconstructionFunctionTemplate PoissonReconstructionFunction(FinalPoints.begin(), FinalPoints.end(), CGAL::make_identity_property_map(PointList::value_type()), CGAL::make_normal_of_point_with_normal_map(PointList::value_type()));

    if (!PoissonReconstructionFunction.compute_implicit_function()) { 
        std::cerr << "Error: Poisson reconstruction failed." << std::endl;
        return EXIT_FAILURE;
    }

    std::cout << "Surface reconstructed." << std::endl;

    // Mesh generation 
    FunctionWrapper = std::bind(&PoissonReconstructionFunctionTemplate::operator(), &PoissonReconstructionFunction, std::placeholders::_1);

    std::cout << "Function wrapped." << std::endl;

	PeriodicMeshDomain Domain = PeriodicMeshDomain::create_implicit_mesh_domain(WrappedFunction, CGAL::Iso_cuboid_3<Kernel>(0.0, 0.0, 0.0, 1.0, 1.0, 1.0));

    std::cout << "Domain constructed." << std::endl;
   
    MeshCriteriaTemplate Criteria(CGAL::parameters::facet_size = FacetSize);

    std::cout << "Criteria constructed." << std::endl;

    MeshFromTriangulationTemplate MeshFromTriangulation = CGAL::make_periodic_3_mesh_3<MeshFromTriangulationTemplate>(Domain, Criteria, CGAL::parameters::no_features(), CGAL::parameters::no_odt(), CGAL::parameters::no_lloyd(), CGAL::parameters::no_perturb(), CGAL::parameters::no_exude());

    /*********
	* Output *
    *********/
    std::ofstream FinalOutputFile(OutputFilename + ".off");
    MeshFromTriangulation.output_boundary_to_off(FinalOutputFile);
    FinalOutputFile.close();

    std::cout << "Meshing concluded." << std::endl;
    return EXIT_SUCCESS;
}

###########
# Imports #
###########
import sys
import argparse
import os
import openmesh
import numpy
import multiprocessing

sys.path.append("/home/martin/Repositories")
import H2Tools

##########################################
# Functions for circle packing algorithm #
##########################################
def AngleFromCenter(x, y, z):
	CosAngle = (numpy.cosh(x + y) * numpy.cosh(x + z) - numpy.cosh(y + z)) / (numpy.sinh(x + y) * numpy.sinh(x + z))
	Angle    = numpy.arccos(CosAngle)
	return Angle

def FlowerSum(RadiusList, CenterID, OrderedListOfNeighbors):
    return sum(AngleFromCenter(RadiusList[CenterID], RadiusList[OrderedListOfNeighbors[i - 1]], RadiusList[OrderedListOfNeighbors[i]]) for i in range(len(OrderedListOfNeighbors)))

def CalculateRadius(i):
	AngleSum = FlowerSum(RadiusList, i, DictOfOrderedNeighbors[i])
	Hat = RadiusList[i] / (1.0 / numpy.sin(AngleSum / (2.0 * len(DictOfOrderedNeighbors[i]))) - 1.0)
	NewRadius = Hat * (1.0 / (numpy.sin(numpy.pi / len(DictOfOrderedNeighbors[i]))) - 1.0)

	return (NewRadius, max(NewRadius / RadiusList[i], RadiusList[i] / NewRadius))

def InitializeRadiusWorker(Argument1, Argument2):
    global DictOfOrderedNeighbors
    global RadiusList

    DictOfOrderedNeighbors = Argument1
    RadiusList = Argument2

def CalculateRadii(DictOfOrderedNeighbors, NumberOfVertices, Precision):
	# Initialize the dictionary
	RadiusList = numpy.array([0.005 for i in range(NumberOfVertices)])

	# Tolerance level
	LastChange = 2.0

	while LastChange > 1.0 + Precision:
		LastChange = 1.0

		MultiprocessingPool = multiprocessing.Pool(processes = 10, initializer = InitializeRadiusWorker, initargs = (DictOfOrderedNeighbors, RadiusList))
		Output = MultiprocessingPool.map(CalculateRadius, [i for i in range(NumberOfVertices)])
		MultiprocessingPool.close()

		LastChange = max([Output[i][1] for i in range(NumberOfVertices)])
		RadiusList = numpy.array([Output[i][0] for i in range(NumberOfVertices)])

		sys.stdout.write("Error: {}\n".format(LastChange - 1.0))

	return RadiusList

def ConstructFlowers(Mesh):
	# Initialize the dictionary
	DictOfOrderedNeighbors = {}
	ListOfVertices = Mesh.vertex_vertex_indices()

	# And populate it using the Mesh structure
	ID = 0

	for Entry in ListOfVertices:
		DictOfOrderedNeighbors[ID] = []

		for Neighbor in Entry:

			if Neighbor >= 0:
				DictOfOrderedNeighbors[ID].append(Neighbor)

		ID += 1

	return DictOfOrderedNeighbors

def DoCirclePacking(Mesh, Precision):
	NumberOfVertices = Mesh.n_vertices()
	Colors           = Mesh.vertex_colors()

	# Construct dictionary of ordered neighbors
	DictOfOrderedNeighbors = ConstructFlowers(Mesh)
	sys.stdout.write("Constructed neighbor lists.\n")

    # The main iteration for finding the correct set of radii
	sys.stdout.write("Calculating circle packing radii.\n")
	RadiusList = CalculateRadii(DictOfOrderedNeighbors, NumberOfVertices, Precision)
	sys.stdout.write("Calculated circle packing radii.\n")

	# Collect info
	HyperbolicDiskPacking = H2Tools.HyperbolicStructure()
	
	for i in range(NumberOfVertices):
		HyperbolicDiskPacking.AddPoint(H2Tools.Point(0.0, i, Colors[i][0], RadiusList[i]))
			
	ListOfEdges = Mesh.edge_vertex_indices()
	
	for i in len(ListOfEdges):
		MeshEdge = ListOfEdges[i]
		HyperbolicDiskPacking.AddEdge(H2Tools.Edge((MeshEdge), i))
		
	sys.stdout.write("Circle packing concluded.\n")
		
	return HyperbolicDiskPacking

########
# Main #
########
if __name__ == "__main__":
	# Assign CLI arguments
	Parser = argparse.ArgumentParser()

	# Path to the datafile
	Parser.add_argument("off", metavar = "Path to data in .off-format", help = "Path to data in .off-format", type = str)
	Parser.add_argument("-spe", metavar = "", help = "Tolerance error in the sphere packing radii", default = 1.0e-7, type = float)

	# Extract arguments
	Arguments      = Parser.parse_args()
	Dataname       = Arguments.off
	ToleranceError = Arguments.spe

	# Name for IO
	Basename = os.path.splitext(Dataname)[0]

	# Import .off-file containing mesh
	Mesh = openmesh.read_trimesh(Dataname)

	# Construct circle packing
	HyperbolicDiskPacking = DoCirclePacking(Mesh, ToleranceError)

	# Output and plot the constructed mesh
	H2Tools.OutputH2P(Basename + ".dis.h2p", HyperbolicDiskPacking)

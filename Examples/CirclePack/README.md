# H2Tools - Disc packing

## Introduction

This folder contains two Python scripts for running the CirclePack algorithm (for packings in hyperbolic two-space).

The CirclePack algorithm is documented here:

Dubejko & Stephenson (1995)  <br />
Circle packing: Experiments in discrete analytic function theory  <br />
Exp. Math. 4, 307-348 

and here:

Collins & Stephenson (2003) <br />
A circle packing algorithm <br />
Comput. Geom. 25, 233-256 

## Usage - First part

The first script, CalculateRadii.py, takes the following inputs:

| Argument | Description                     | Default |
| -------- | ------------------------------- | ------- |
| -off     | Path to the mesh in .off-format | -       |
| -spe     | Convergence criteria            | 1.0e-7  |

and outputs the calculated radii as a file in .h2p-format (without any positions associated to the points).

## Usage - Second part

The second script, CalculatePositions.py, takes the following inputs:

| Argument | Description                                          | Default |
| -------- | ---------------------------------------------------- | ------- |
| -off     | Path to the original mesh in .off-format             | -       |
| -h2p     | Path to the file containing the radii in .h2p-format | -       |

and outputs a plot of the final packing along with the packing in .h2p-format.

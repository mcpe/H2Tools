###########
# Imports #
###########
import sys
import argparse
import os
import openmesh
import math
import cmath
import numpy

sys.path.append("/home/martin/Repositories")
import H2Tools

#######################################################
# Functions for computing positions of vertices in H2 #
#######################################################
def AngleFromCenter(x, y, z):
	CosAngle = (numpy.cosh(x + y) * numpy.cosh(x + z) - numpy.cosh(y + z)) / (numpy.sinh(x + y) * numpy.sinh(x + z))
	Angle    = numpy.arccos(CosAngle)
	return Angle
	
def CalculatePositions(Mesh, DictOfOrderedNeighbors, RadiusList):
	NumberOfVertices = Mesh.n_vertices()

	# Initialize the dictionary
	Positions = {}
	
	for i in range(NumberOfVertices):
		Positions[i] = 0.0

	# Place first edge manually on the real axis
	Node1 = 0
	Node2 = DictOfOrderedNeighbors[Node1][0]
	
	Positions[Node1]  = 0.0
	Distance          = RadiusList[Node1] + RadiusList[Node2]
	EuclideanDistance = ((math.cosh(Distance) - 1.0) / (math.cosh(Distance) + 1.0))**0.5
	Positions[Node2]  = EuclideanDistance

	ListOfPlacedNodes = [Node1, Node2]
	NewlyPlacedNodes  = [Node1, Node2]

	# Place the rest
	while len(ListOfPlacedNodes) < NumberOfVertices:
		sys.stdout.write("Placed {} of {} vertices.\n".format(len(ListOfPlacedNodes), NumberOfVertices))

		NewNodes = []

		for Vertex in NewlyPlacedNodes:
			Neighbors = DictOfOrderedNeighbors[Vertex]

			ReflectionCenter = H2Tools.Point(H2Tools.InterpolatePosition(H2Tools.Point(0.0), H2Tools.Point(Positions[Vertex]), 0.5))

			for i in range(-len(Neighbors), len(Neighbors) - 1):

				if Neighbors[i] in ListOfPlacedNodes and Neighbors[i + 1] not in ListOfPlacedNodes:
					PositionedNeighbor    = Neighbors[i]
					NonPositionedNeighbor = Neighbors[i + 1]

					if H2Tools.Distance(H2Tools.Point(Positions[Vertex]), H2Tools.Point(Positions[PositionedNeighbor])) < 1.05 * (RadiusList[Vertex] + RadiusList[PositionedNeighbor]):

						# Angle between circles
						Theta = AngleFromCenter(RadiusList[Vertex], RadiusList[PositionedNeighbor], RadiusList[NonPositionedNeighbor])

						# Vertex moved to center - and neighbor moved with it
						ReflectedPositionedNeighbor = H2Tools.Point(H2Tools.InterpolatePosition(H2Tools.Point(Positions[PositionedNeighbor]), ReflectionCenter, 2.0))
						Angle = cmath.phase(ReflectedPositionedNeighbor.Position)

						# Place the new node
						TotalAngle                     = (Angle + Theta) % (2.0 * numpy.pi)
						Distance                       = RadiusList[Vertex] + RadiusList[NonPositionedNeighbor]
						EuclideanDistance              = ((math.cosh(Distance) - 1.0) / (math.cosh(Distance) + 1.0))**0.5
						ReflectedNonPositionedNeighbor = cmath.rect(EuclideanDistance, TotalAngle)

						# And reflect it back
						NewPosition = H2Tools.InterpolatePosition(H2Tools.Point(ReflectedNonPositionedNeighbor), ReflectionCenter, 2.0)

						# And store the new position
						Positions[NonPositionedNeighbor] = NewPosition
						ListOfPlacedNodes.append(NonPositionedNeighbor)
						NewNodes.append(NonPositionedNeighbor)

		NewlyPlacedNodes = list(NewNodes)

	sys.stdout.write("Placed {} of {} vertices.\n".format(len(ListOfPlacedNodes), NumberOfVertices))

	return Positions
	
def ConstructFlowers(Mesh):
	# Initialize the dictionary
	DictOfOrderedNeighbors = {}
	ListOfVertices = Mesh.vertex_vertex_indices()

	# And populate it using the Mesh structure
	ID = 0

	for Entry in ListOfVertices:
		DictOfOrderedNeighbors[ID] = []

		for Neighbor in Entry:

			if Neighbor >= 0:
				DictOfOrderedNeighbors[ID].append(Neighbor)

		ID += 1

	return DictOfOrderedNeighbors

########
# Main #
########
if __name__ == "__main__":
	# Assign CLI arguments
	Parser = argparse.ArgumentParser()

	# Path to the datafile
	Parser.add_argument("off", metavar = "Path to data in .off-format", help = "Path to data in .off-format", type = str)
	Parser.add_argument("h2p", metavar = "Path to data in .h2p-format", help = "Path to data in .h2p-format", type = str)

	# Extract arguments
	Arguments      = Parser.parse_args()
	Dataname       = Arguments.off
	Packingname    = Arguments.h2p

	# Name for IO
	Basename = os.path.splitext(Dataname)[0]

	# Import .off-file containing mesh and .h2p-file containing packings (only radii)
	Mesh    = openmesh.read_trimesh(Dataname)
	Packing = H2Tools.ImportH2P(Packingname)

	# Calculate positions
	NumberOfVertices = Mesh.n_vertices()
		
	DictOfOrderedNeighbors = ConstructFlowers(Mesh)
	RadiusList = [Packing.Points[i].Radius for i in range(NumberOfVertices)]
	Positions = CalculatePositions(Mesh, DictOfOrderedNeighbors, RadiusList)
	sys.stdout.write("Calculated circle packing positions.\n")
	
	for i in range(NumberOfVertices):
		Packing.Points[i].SetPosition(Positions[i])

	# Output and plot the constructed mesh
	H2Tools.OutputH2P(Basename + ".pos.h2p", Packing)
	H2Tools.PlotPoincareDisk(Basename + ".png", Packing)

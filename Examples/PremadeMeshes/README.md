# H2Tools - Premade meshes

## Introduction

This folder contains the three meshes in euclidean three-space (in .off-format) and the three corresponding meshes in hyperbolic two-space (in .h2p-format) shown in Figure 4 in:

Name<br>
Pedersen, Hyde, Ramsden, and Kirkensgaard<br>
Journal

The IDs of the vertices in the two meshes match, making the two files suitable for visualizing decorations of one as a decoration of the other.

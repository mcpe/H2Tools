from setuptools import setup

setup(
    name = 'H2Tools',
    version = '0.1.0',    
    description = 'A Python module for constructing, manipulating and visualizing patterns in the Poincare disc model of hyperbolic two-space',
    url = 'https://gitlab.com/mcpe/H2Tools',
    author = 'Martin Cramer Pedersen',
    license = 'GNU General Public License v3.0',
    packages = ['H2Tools'],
    install_requires = ['matplotlib', 'numpy'],
)
